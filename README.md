# Ali Twitter, Virtualization Approach

Twitter-like web app used Rails-Framemwork which can do:

1. Create a tweet
2. Delete a tweet
3. List tweets

# Architecture
![Architecture](./architecture.jpg)

# Deployment

## Dependencies
1. Virtualbox
2. Vagrant
3. Ansible

## How to Install

### 1. Spawn new virtual machines using vagrant config from `Vagrantfile`
```
vagrant up
```

Virtual Machines that will be spawned are the following:

| Name | User | Password | IP Address | Description |
|-|-|-|-|-|
| app1 | vagrant | vagrant | 10.10.0.7 | App 1 VM |
| app2 |vagrant| vagrant | 10.10.0.6 | App 2 VM |
| database | vagrant | vagrant | 10.10.0.2 | Database VM |
| haproxy |vagrant| vagrant | 10.10.0.3 | Load Balancer VM |
| runner |vagrant| vagrant | 10.10.0.8 | Gitlab Runner VM |

### 2. Copy ansible control node (local machine) public key to each VM
```
ssh-copy-id <user>@<IP>
```

### 3. Provision Virtual Machines
This will install all required dependencies for App, Database, and Load Balancer virtual machines

If a password is required, input `vagrant` (not required if  already copy control node's (local machine) public key to each VM)
```
ansible-playbook -u vagrant -i ansible/inventory/hosts ansible/install.yml
```

### 4. Prepare artifact
This process is actually automated on Gitlab CI / CD, but because this assignment is submitted as `.zip`, the following steps are required

1. Archive file to `.tar`
```
 tar -czf package.tar *
```

2. Copy artifact to `app1` and `app2` virtual machines
```
scp package.tar vagrant@<ip>:/home/vagrant/package.tar
```

### 5. Deploy App
Deploy app using ansible

If a password is required, input `vagrant` (not required if  already copy control node's (local machine) public key to each VM)
```
ansible-playbook -u vagrant -i ansible/inventory/hosts ansible/deploy.yml
```

### 6. Access App
Open from browser `10.10.0.3/tweets`